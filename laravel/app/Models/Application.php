<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Redis;

class Application extends Model
{
    protected $table = 'applications';
    protected $primaryKey = 'id';

    protected $guarded = ['id'];


    /**
     * Find record by application id 
     *
     * @return Application
     */
    protected function findByApplicationId($application_id){
        return Application::where('application_id',$application_id)->first();        
    }

    /**
     * Will check the id if exist will random again
     *
     * @return string
     */
    protected function getUniqueId()
    {                
        $finalString = $this->generateRandomString(env('LENGTH_UNIQUE_ID', 4));
        if (Redis::get("application_id.{$finalString}")) {
            //Find Key in redis first if exist let generate again
            return $this->getUniqueId();
        } else {
            Redis::set("application_id.{$finalString}", true);
            //check again in database to ensure the string is not exist.            
            return $this::where('application_id', $finalString)->first() ? $this->getUniqueId() : $finalString;
        }
    }

    private function generateRandomString($length = 10) {
        $characters = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ';        
        $charactersLength = strlen($characters);
        $randomString = '';
        for ($i = 0; $i < $length; $i++) {
            $randomString .= $characters[rand(0, $charactersLength - 1)];
        }
        return $randomString;
    }

    /**
     * The "booted" method of the model.
     *
     * @return void
     */
    protected static function booted()
    {

        static::created(function ($application) {
            $application->application_id = $application->getUniqueId();
            $application->save();
        });
    }
}
