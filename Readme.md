### Installation

- Create directory "database" 
- Start application with docker-compose 
- Let access to the database container and create database name "laravel"
- Let access to the laravel container then run composer install and migrate


### Challenge 
 - /laravel/database/migrations/2021_02_13_052923_create_table_applications.php
 - /laravel/app/Http/Controllers/Challenge/ChallengeController.php
 - /laravel/app/Models/Application.php